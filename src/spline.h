#ifndef SPLINE_SPLINE_H
#define SPLINE_SPLINE_H

#include "point.h"

point **spline(point **initials, size_t n_initials, float dt, size_t *n_generated);

float *spline_1d(float *initials, size_t n_initials, float dt, size_t *n_generated);

#endif //SPLINE_SPLINE_H
