#include <stdlib.h>
#include <stdio.h>

#include "spline.h"

int main(int argc, char **argv) {
    /**
     * 3D spline:
     */

//    const size_t n_initials = 7;
//    point **initials = (point**) malloc(sizeof(point*) * n_initials);
//
//    initials[0] = point_create(0, 5, 0);
//    initials[1] = point_create(0, 5, 0);
//    initials[2] = point_create(15, 10, 15);
//    initials[3] = point_create(45, 5, 30);
//    initials[4] = point_create(65, 15, 45);
//    initials[5] = point_create(80, 10, 70);
//    initials[6] = point_create(80, 10, 70);
//
//    size_t n_generated;
//    point **generated = spline(initials, n_initials, 0.01, &n_generated);
//
//    printf("GENERATED: %zu \n", n_generated);
//
//    for (int i = 0; i < n_initials; i++) {
//        printf("x: %f, y: %f, z: %f \n", point_get_x(generated[i]), point_get_y(generated[i]), point_get_z(generated[i]));
//    }
//
//    /*
//     * LOGGING:
//     */
//
//    FILE *f_out = fopen("curve.txt", "w");
//    if (f_out == NULL) {
//        return 1;
//    }
//    for (int i = 0; i < n_generated; i++) {
//        fprintf(f_out, "%f %f %f\n", point_get_x(generated[i]), point_get_y(generated[i]), point_get_z(generated[i]));
//    }
//    fclose(f_out);
//
//    FILE *f_initial = fopen("initial.txt", "w");
//    if (f_initial == NULL) {
//        return 1;
//    }
//    for (int i = 0; i < n_initials; i++) {
//        fprintf(f_initial, "%f %f %f\n", point_get_x(initials[i]), point_get_y(initials[i]), point_get_z(initials[i]));
//    }
//    fclose(f_initial);

    /**
     * 1D spline:
     */

    const size_t n_initials = 9;
    float *initials = (float*) malloc(sizeof(float) * n_initials);

    initials[0] = 0;
    initials[1] = 0;
    initials[2] = 2;
    initials[3] = 3;
    initials[4] = 9;
    initials[5] = 5;
    initials[6] = 1;
    initials[7] = -2;
    initials[8] = -2;

    size_t n_generated;
    float *generated = spline_1d(initials, n_initials, 0.01, &n_generated);

    printf("GENERATED: %zu \n", n_generated);

    for (int i = 0; i < n_initials; i++) {
        printf("x: %f \n", generated[i]);
    }

    /*
     * LOGGING:
     */

    FILE *f_out = fopen("curve.txt", "w");
    if (f_out == NULL) {
        return 1;
    }
    for (int i = 0; i < n_generated; i++) {
        fprintf(f_out, "%f \n", generated[i]);
    }
    fclose(f_out);

    FILE *f_initial = fopen("initial.txt", "w");
    if (f_initial == NULL) {
        return 1;
    }
    for (int i = 0; i < n_initials; i++) {
        fprintf(f_initial, "%f \n", initials[i]);
    }
    fclose(f_initial);

    return 0;
}
