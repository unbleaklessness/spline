#include "spline.h"

point **spline(point **initials, size_t n_initials, float dt, size_t *n_generated) {
    size_t n_new_per_iteration = (size_t) (1.0f / dt);
    size_t n_new = n_new_per_iteration * (n_initials - 2);

    *n_generated = n_new;
    point **result = (point**) malloc(sizeof(point*) * n_new);
    if (result == NULL) {
        return NULL;
    }

    int j = 0;
    for (size_t i = 0; i < n_initials - 2; i++) {
        float t = 0.0f;
        while (t < 1 && j < n_new) {

            point *p0 = initials[i];
            point *p1 = initials[i + 1];
            point *p2 = initials[i + 2];

            point *p0m = point_add(p0, p1);
            point_scalar_multiply_l(p0m, 0.5f);

            point *p2m = point_add(p2, p1);
            point_scalar_multiply_l(p2m, 0.5f);

            point *a1 = point_scalar_multiply(p1, t);
            point *a2 = point_scalar_multiply(p0m, 1.0f - t);
            point *a3 = point_add(a1, a2);
            point *a4 = point_scalar_multiply(a3, 1.0f - t);

            point *a5 = point_scalar_multiply(p1, 1.0f - t);
            point *a6 = point_scalar_multiply(p2m, t);
            point *a7 = point_add(a5, a6);
            point *a8 = point_scalar_multiply(a7, t);

            result[j] = point_add(a4, a8);

            free(a1);
            free(a2);
            free(a3);
            free(a4);
            free(a5);
            free(a6);
            free(a7);
            free(a8);

            free(p0m);
            free(p2m);

            t += dt;
            j += 1;
        }
    }

    return result;
}

float *spline_1d(float *initials, size_t n_initials, float dt, size_t *n_generated) {
    size_t n_new_per_iteration = (size_t) (1.0f / dt);
    size_t n_new = n_new_per_iteration * (n_initials - 2);

    *n_generated = n_new;
    float *result = (float*) malloc(sizeof(float) * n_new);
    if (result == NULL) {
        return NULL;
    }

    int j = 0;
    for (size_t i = 0; i < n_initials - 2; i++) {
        float t = 0.0f;
        while (t < 1 && j < n_new) {

            float p0 = initials[i];
            float p1 = initials[i + 1];
            float p2 = initials[i + 2];

            float p0m = (p0 + p1) /  2.0f;
            float p2m = (p2 + p1) /  2.0f;

            result[j] = (2 * t * t * t - 3 * t * t + 1) * p0 + (t * t * t - 2 * t * t + t) * p0m + (-2 * t * t * t + 3 * t * t) * p1 + (t * t * t - t * t) * p2m;

            t += dt;
            j += 1;
        }
    }

    return result;
}