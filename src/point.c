#include "point.h"

#include "string.h"

typedef struct point {
    float x;
    float y;
    float z;
} point;

point *point_create(float x, float y, float z) {
    point *result = (point*) malloc(sizeof(point));
    if (result == NULL) {
        return NULL;
    }
    result->x = x;
    result->y = y;
    result->z = z;
    return result;
}

void point_destroy(point *p) {
    free(p);
}

float point_get_x(const point *p) { return p->x; }
float point_get_y(const point *p) { return p->y; }
float point_get_z(const point *p) { return p->z; }

point *point_copy(const point *p) {
    point *result = (point*) malloc(sizeof(point));
    if (result == NULL) {
        return NULL;
    }
    memcpy(result, p, sizeof(point));
    return result;
}

point *point_add(const point *a, const point *b) {
    point *result = (point*) malloc(sizeof(point));
    if (result == NULL) {
        return NULL;
    }
    result->x = a->x + b->x;
    result->y = a->y + b->y;
    result->z = a->z + b->z;
    return result;
}

int point_add_l(point *a, const point *b) {
    a->x += b->x;
    a->y += b->y;
    a->z += b->z;
    return 1;
}

point *point_multiply(const point *a, const point *b) {
    point *result = (point*) malloc(sizeof(point));
    if (result == NULL) {
        return NULL;
    }
    result->x = a->x * b->x;
    result->y = a->y * b->y;
    result->z = a->z * b->z;
    return result;
}

int point_multiply_l(point *a, const point *b) {
    a->x *= b->x;
    a->y *= b->y;
    a->z *= b->z;
    return 1;
}

point *point_scalar_multiply(const point *a, float n) {
    point *result = (point*) malloc(sizeof(point));
    if (result == NULL) {
        return NULL;
    }
    result->x = a->x * n;
    result->y = a->y * n;
    result->z = a->z * n;
    return result;
}

int point_scalar_multiply_l(point *a, float n) {
    a->x *= n;
    a->y *= n;
    a->z *= n;
    return 1;
}
