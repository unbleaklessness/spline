#ifndef SPLINE_POINT_H
#define SPLINE_POINT_H

#include "stdlib.h"

typedef struct point point;

point *point_create(float x, float y, float z);
void point_destroy(point *p);

float point_get_x(const point *p);
float point_get_y(const point *p);
float point_get_z(const point *p);

point *point_copy(const point *p);

point *point_add(const point *a, const point *b);

int point_add_l(point *a, const point *b);

point *point_multiply(const point *a, const point *b);

int point_multiply_l(point *a, const point *b);

point *point_scalar_multiply(const point *a, float n);

int point_scalar_multiply_l(point *a, float n);

#endif //SPLINE_POINT_H
